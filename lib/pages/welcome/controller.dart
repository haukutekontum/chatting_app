import 'package:firebase_chat/pages/welcome/state.dart';
import 'package:get/get.dart';

class WelcomeController extends GetxController{
  final state = WelcomeState();
  WelcomeController();
}